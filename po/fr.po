# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the teleports.ubports package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: teleports.ubports\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-23 22:06+0000\n"
"PO-Revision-Date: 2020-08-17 12:51+0000\n"
"Last-Translator: Anne Onyme <anneonyme017@netcourrier.com>\n"
"Language-Team: French <https://translate.ubports.com/projects/ubports/"
"teleports/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.11.3\n"

#: ../app/qml/components/CountryPicker.qml:21
#: ../app/qml/pages/WaitPhoneNumberPage.qml:36
msgid "Choose a country"
msgstr "Choisissez un pays"

#: ../app/qml/components/CountryPicker.qml:38
msgid "Search country name..."
msgstr "Rechercher le nom d'un pays..."

#: ../app/qml/components/DeleteDialog.qml:9
msgid ""
"The message will be deleted for all users in the chat. Do you really want to "
"delete it?"
msgstr ""
"Ce message ne sera plus visible pour tous les membres de la discussion. "
"Voulez-vous vraiment le supprimer ?"

#: ../app/qml/components/DeleteDialog.qml:10
msgid ""
"The message will be deleted only for you. Do you really want to delete it?"
msgstr ""
"Ce message ne sera supprimé que pour vous. Voulez-vous vraiment le "
"supprimer ?"

#: ../app/qml/components/DeleteDialog.qml:12
#: ../app/qml/delegates/MessageBubbleItem.qml:37
#: ../app/qml/pages/SettingsPage.qml:152 ../app/qml/pages/UserListPage.qml:104
#: ../app/qml/pages/UserListPage.qml:198
msgid "Delete"
msgstr "Supprimer"

#: ../app/qml/components/InputInfoBox.qml:91
msgid "Edit message"
msgstr "Modifier le message"

#: ../app/qml/components/MessageStatusRow.qml:36
msgid "Edited"
msgstr "Modifié"

#: ../app/qml/components/PopupDialog.qml:14
msgid "Okay"
msgstr "Valider"

#: ../app/qml/components/PopupDialog.qml:15
#: ../app/qml/components/PopupWaitCancel.qml:13
#: ../app/qml/pages/ChatListPage.qml:31
msgid "Cancel"
msgstr "Annuler"

#: ../app/qml/components/UserProfile.qml:72
msgid "Members: %1"
msgstr "Membres : %1"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:6
msgid "Channel called << %1 >> created"
msgstr "La chaîne appelée << %1 >> a été créée"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:7
msgid "%1 created a group called << %2 >>"
msgstr "%1 a créé un groupe appelé << %2 >>"

#: ../app/qml/delegates/MessageBubbleItem.qml:49
msgid "Copy"
msgstr "Copier"

#: ../app/qml/delegates/MessageBubbleItem.qml:59
#: ../app/qml/pages/ChatInfoPage.qml:35
msgid "Edit"
msgstr "Modifier"

#: ../app/qml/delegates/MessageBubbleItem.qml:67
msgid "Reply"
msgstr "Répondre"

#: ../app/qml/delegates/MessageBubbleItem.qml:73
msgid "Sticker Pack info"
msgstr "Informations au sujet du pack d'autocollants"

#: ../app/qml/delegates/MessageBubbleItem.qml:79
#: ../app/qml/pages/ChatListPage.qml:361
msgid "Forward"
msgstr "Transférer"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:11
msgid "%1 joined the group"
msgstr "%1 a rejoint le groupe"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:12
msgid "%1 added %2"
msgstr "%1 a ajouté %2"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:35
msgid "%1 user(s)"
msgid_plural ""
msgstr[0] "%1 utilisateur/trice"
msgstr[1] "%1 utilisateurs/trices"

#: ../app/qml/delegates/MessageChatDeleteMember.qml:6
msgid "%1 left the group"
msgstr "%1 a quitté le groupe"

#. TRANSLATORS: Notification message saying: person A removed person B (from a group)
#: ../app/qml/delegates/MessageChatDeleteMember.qml:7
#: ../push/pushhelper.cpp:238
msgid "%1 removed %2"
msgstr "%1 a supprimé %2"

#: ../app/qml/delegates/MessageChatUpgradeFrom.qml:4
#: ../app/qml/delegates/MessageChatUpgradeTo.qml:4
msgid "Group has been upgraded to Supergroup"
msgstr "Le groupe a été transformé en supergroupe"

#: ../app/qml/delegates/MessageContactRegistered.qml:5
msgid "%1 has joined Telegram!"
msgstr "%1 a rejoint Telegram !"

#: ../app/qml/delegates/MessageContentBase.qml:37
msgid "Forwarded from %1"
msgstr "Transféré par %1"

#. TRANSLATORS: This is the duration of a phone call in hours:minutes:seconds format
#: ../app/qml/delegates/MessageContentCall.qml:59
msgid "Duration: %1:%2:%3"
msgstr "Durée : %1:%2:%3"

#: ../app/qml/delegates/MessageContentVoiceNote.qml:56
msgid "Voice note"
msgstr "Note vocale"

#: ../app/qml/delegates/MessageJoinByLink.qml:5
msgid "%1 joined by invite link"
msgstr "%1 vous a rejoint via un lien d'invitation"

#: ../app/qml/delegates/MessageScreenshotTaken.qml:4
msgid "A screenshot has been taken"
msgstr "Une capture d'écran a été prise"

#: ../app/qml/delegates/MessageStickerItem.qml:20
msgid "Animated stickers not supported yet :("
msgstr "Les autocollants animés ne sont pas encore pris en charge :("

#: ../app/qml/delegates/MessageUnreadLabelItem.qml:4
msgid "Missing label..."
msgstr "Étiquette manquante..."

#: ../app/qml/delegates/MessageUnsupported.qml:4
#: ../libs/qtdlib/messages/content/qtdmessageunsupported.cpp:12
msgid "Unsupported message"
msgstr "Message non supporté"

#: ../app/qml/delegates/NotImplementedYet.qml:12
msgid "Unknown message type, see logfile for details..."
msgstr ""
"Type de message inconnu, consultez les journaux pour plus de détails..."

#: ../app/qml/middleware/ChatMiddleware.qml:21
msgid "Are you sure you want to clear the history?"
msgstr "Voulez-vous vraiment supprimer l'historique ?"

#: ../app/qml/middleware/ChatMiddleware.qml:22
#: ../app/qml/pages/ChatListPage.qml:131
msgid "Clear history"
msgstr "Supprimer l'historique"

#: ../app/qml/middleware/ChatMiddleware.qml:31
msgid "Are you sure you want to leave this chat?"
msgstr "Voulez-vous vraiment quitter cette discussion ?"

#: ../app/qml/middleware/ChatMiddleware.qml:32
msgid "Leave"
msgstr "Quitter"

#: ../app/qml/middleware/ErrorsMiddleware.qml:19
msgid "Close"
msgstr "Fermer"

#: ../app/qml/pages/AboutPage.qml:13 ../app/qml/pages/ChatListPage.qml:83
msgid "About"
msgstr "À propos"

#: ../app/qml/pages/AboutPage.qml:24 ../app/qml/pages/ChatInfoPage.qml:26
#: ../app/qml/pages/ConnectivityPage.qml:46
#: ../app/qml/pages/SecretChatKeyHashPage.qml:18
#: ../app/qml/pages/SettingsPage.qml:33 ../app/qml/pages/UserListPage.qml:29
msgid "Back"
msgstr "Retour"

#. TRANSLATORS: Application name.
#: ../app/qml/pages/AboutPage.qml:66 ../app/qml/pages/ChatListPage.qml:21
#: ../push/pushhelper.cpp:114 teleports.desktop.in.h:1
msgid "TELEports"
msgstr "TELEports"

#: ../app/qml/pages/AboutPage.qml:72
msgid "Version %1"
msgstr "Version %1"

#: ../app/qml/pages/AboutPage.qml:74
msgid " (git# %1)"
msgstr " (git# %1)"

#: ../app/qml/pages/AboutPage.qml:93
msgid "Get the source"
msgstr "Récupérer le code source"

#: ../app/qml/pages/AboutPage.qml:94
msgid "Report issues"
msgstr "Signaler les problèmes"

#: ../app/qml/pages/AboutPage.qml:95
msgid "Help translate"
msgstr "Aider à traduire"

#: ../app/qml/pages/ChatInfoPage.qml:21
msgid "Group Details"
msgstr "Détails du groupe"

#: ../app/qml/pages/ChatInfoPage.qml:21
msgid "Profile"
msgstr "Profil"

#: ../app/qml/pages/ChatInfoPage.qml:41
msgid "Send message"
msgstr "Envoyer le message"

#: ../app/qml/pages/ChatInfoPage.qml:61
msgid "Edit user data and press Save"
msgstr "Modifiez les données de l'utilisateur et appuyez sur Enregistrer"

#: ../app/qml/pages/ChatInfoPage.qml:63 ../app/qml/pages/PreviewPage.qml:49
msgid "Save"
msgstr "Enregistrer"

#: ../app/qml/pages/ChatInfoPage.qml:74 ../app/qml/pages/UserListPage.qml:59
msgid "Phone no"
msgstr "Numéro de téléphone"

#: ../app/qml/pages/ChatInfoPage.qml:83 ../app/qml/pages/UserListPage.qml:67
msgid "First name"
msgstr "Prénom"

#: ../app/qml/pages/ChatInfoPage.qml:92 ../app/qml/pages/UserListPage.qml:75
msgid "Last name"
msgstr "Nom"

#: ../app/qml/pages/ChatInfoPage.qml:131
msgid "Notifications"
msgstr "Notifications"

#: ../app/qml/pages/ChatInfoPage.qml:160
msgid "%1 group in common"
msgid_plural "%1 groups in common"
msgstr[0] "%1 groupe en commun"
msgstr[1] "%1 groupes en commun"

#: ../app/qml/pages/ChatInfoPage.qml:185
#: ../app/qml/pages/SecretChatKeyHashPage.qml:13
msgid "Encryption Key"
msgstr "Clé de chiffrement"

#: ../app/qml/pages/ChatListPage.qml:20
msgid "Select destination or cancel..."
msgstr "Sélectionnez une destination ou annulez..."

#: ../app/qml/pages/ChatListPage.qml:37 ../app/qml/pages/ChatListPage.qml:64
#: ../app/qml/pages/SettingsPage.qml:22
msgid "Settings"
msgstr "Paramètres"

#: ../app/qml/pages/ChatListPage.qml:54 ../libs/qtdlib/chat/qtdchat.cpp:84
msgid "Saved Messages"
msgstr "Messages enregistrés"

#: ../app/qml/pages/ChatListPage.qml:59 ../app/qml/pages/UserListPage.qml:18
msgid "Contacts"
msgstr "Contacts"

#: ../app/qml/pages/ChatListPage.qml:70
msgid "Night mode"
msgstr "Mode nuit"

#: ../app/qml/pages/ChatListPage.qml:126
msgid "Leave chat"
msgstr "Quitter la discussion"

#: ../app/qml/pages/ChatListPage.qml:142 ../app/qml/pages/UserListPage.qml:114
msgid "Info"
msgstr "Informations"

#: ../app/qml/pages/ChatListPage.qml:359
msgid "Do you want to forward the selected messages to %1?"
msgstr "Voulez-vous transférer le message sélectionné à %1 ?"

#: ../app/qml/pages/ChatListPage.qml:374 ../app/qml/pages/ChatListPage.qml:398
msgid "Enter optional message..."
msgstr "Saisissez un message (facultatif)..."

#: ../app/qml/pages/ChatListPage.qml:382
msgid "Do you want to send the imported files to %1?"
msgstr "Voulez-vous envoyer les fichiers importés à %1 ?"

#: ../app/qml/pages/ChatListPage.qml:384
#: ../app/qml/pages/MessageListPage.qml:628
msgid "Send"
msgstr "Envoyer"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:28
msgid "Connecting"
msgstr "Connexion"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:26
msgid "Offline"
msgstr "Déconnecté(e)"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:30
#: ../libs/qtdlib/user/qtduserstatus.cpp:78
msgid "Online"
msgstr "Connecté(e)"

#: ../app/qml/pages/ConnectivityPage.qml:27
msgid "Connecting To Proxy"
msgstr "Connexion au proxy"

#: ../app/qml/pages/ConnectivityPage.qml:29
msgid "Updating"
msgstr "Mise à jour"

#: ../app/qml/pages/ConnectivityPage.qml:35
msgid "Connectivity"
msgstr "Connectivité"

#: ../app/qml/pages/ConnectivityPage.qml:74
msgid "Telegram connectivity status:"
msgstr "Statut de connectivité de Telegram :"

#: ../app/qml/pages/ConnectivityPage.qml:81
msgid "Ubuntu Touch connectivity status:"
msgstr "Statut de connectivité d'Ubuntu Touch :"

#: ../app/qml/pages/ConnectivityPage.qml:88
msgid "Ubuntu Touch bandwith limited"
msgstr "Bande passante d'Ubuntu Touch limitée"

#: ../app/qml/pages/ConnectivityPage.qml:88
msgid "Ubuntu Touch bandwith not limited"
msgstr "Bande passante d'Ubuntu Touch illimitée"

#: ../app/qml/pages/MessageListPage.qml:31
msgid "%1 member"
msgid_plural "%1 members"
msgstr[0] "%1 membre"
msgstr[1] "%1 membres"

#: ../app/qml/pages/MessageListPage.qml:33
msgid ", %1 online"
msgid_plural ", %1 online"
msgstr[0] ", %1 connecté(e)"
msgstr[1] ", %1 connecté(e)s"

#: ../app/qml/pages/MessageListPage.qml:130
msgid "Telegram"
msgstr "Telegram"

#: ../app/qml/pages/MessageListPage.qml:333
msgid "You are not allowed to post in this channel"
msgstr "Vous n'êtes pas autorisé à publier sur cette chaîne"

#: ../app/qml/pages/MessageListPage.qml:337
msgid "Waiting for other party to accept the secret chat..."
msgstr ""
"Attente de l'acceptation de la discussion secrète par les autres membres..."

#: ../app/qml/pages/MessageListPage.qml:339
msgid "Secret chat has been closed"
msgstr "La discussion secrète a été fermée"

#: ../app/qml/pages/MessageListPage.qml:464
msgid "Type a message..."
msgstr "Saisissez un message..."

#: ../app/qml/pages/MessageListPage.qml:626
msgid "Do you want to share your location with %1?"
msgstr "Voulez-vous partager votre position avec %1 ?"

#: ../app/qml/pages/MessageListPage.qml:639
msgid "Requesting location from OS..."
msgstr "Demande de la position au système d'exploitation..."

#: ../app/qml/pages/PickerPage.qml:16
msgid "Content Picker"
msgstr "Sélecteur de contenu"

#: ../app/qml/pages/PreviewPage.qml:35
msgid "File: "
msgstr "Fichier : "

#: ../app/qml/pages/SecretChatKeyHashPage.qml:65
msgid ""
"Check the image or the text. If they match with the ones on <b>%1</b>'s "
"device, end-to-end cryptography is granted."
msgstr ""
"Vérifiez l'image ou le texte. S'ils correspondent à ceux sur l'appareil de "
"<b>%1</b>, le chiffrement de bout en bout est accepté."

#: ../app/qml/pages/SettingsPage.qml:70 ../app/qml/pages/SettingsPage.qml:142
msgid "Logout"
msgstr "Se déconnecter"

#: ../app/qml/pages/SettingsPage.qml:86
msgid "Delete account"
msgstr "Supprimer le compte"

#: ../app/qml/pages/SettingsPage.qml:102
msgid "Connectivity status"
msgstr "Statut de connectivité"

#: ../app/qml/pages/SettingsPage.qml:119
msgid "Toggle message status indicators"
msgstr "Basculer les indicateurs de statut des messages"

#: ../app/qml/pages/SettingsPage.qml:140
msgid ""
"Warning: Logging out will delete all local data from this device, including "
"secret chats. Are you still sure you want to log out?"
msgstr ""
"Attention : une déconnexion supprimera toutes les données en local sur cet "
"appareil, y compris les discussions secrètes. Voulez-vous vraiment vous "
"déconnecter ?"

#: ../app/qml/pages/SettingsPage.qml:150
msgid ""
"Warning: Deleting the account will delete all the data you ever received or "
"send using telegram except for data you have explicitly saved outside the "
"telegram cloud. Are you really really sure you want to delete your telegram "
"account?"
msgstr ""
"Attention : la suppression de ce compte supprimera toutes les données que "
"vous avez reçues ou envoyées en utilisant Telegram, à l'exception des "
"données que vous avez explicitement enregistrées en dehors des serveurs de "
"Telegram. Voulez-vous vraiment supprimer votre compte Telegram ?"

#: ../app/qml/pages/UserListPage.qml:38
msgid "Add Contact"
msgstr "Ajouter un contact"

#: ../app/qml/pages/UserListPage.qml:47
msgid "The contact will be added. First and last name are optional"
msgstr "Le contact sera ajouté. Le prénom et le nom sont facultatifs."

#: ../app/qml/pages/UserListPage.qml:49
msgid "Add"
msgstr "Ajouter"

#: ../app/qml/pages/UserListPage.qml:119
msgid "Secret Chat"
msgstr "Discussion secrète"

#: ../app/qml/pages/UserListPage.qml:196
msgid "The contact will be deleted. Are you sure?"
msgstr "Voulez-vous vraiment supprimer le contact ?"

#: ../app/qml/pages/WaitCodePage.qml:17
msgid "Enter Code"
msgstr "Saisissez un code"

#: ../app/qml/pages/WaitCodePage.qml:39
msgid "Code"
msgstr "Code"

#: ../app/qml/pages/WaitCodePage.qml:54
msgid "We've send a code via telegram to your device. Please enter it here."
msgstr ""
"Nous avons envoyé un code à votre appareil via Telegram. Veuillez le saisir "
"ici."

#: ../app/qml/pages/WaitPasswordPage.qml:18
msgid "Enter Password"
msgstr "Saisissez un mot de passe"

#: ../app/qml/pages/WaitPasswordPage.qml:39
msgid "Password"
msgstr "Mot de passe"

#: ../app/qml/pages/WaitPasswordPage.qml:62
#: ../app/qml/pages/WaitPhoneNumberPage.qml:92
#: ../app/qml/pages/WaitRegistrationPage.qml:57
msgid "Next..."
msgstr "Suivant..."

#: ../app/qml/pages/WaitPhoneNumberPage.qml:17
msgid "Enter Phone Number"
msgstr "Composez un numéro de téléphone"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:54
#: ../app/qml/pages/WaitPhoneNumberPage.qml:64
msgid "Phone number"
msgstr "Numéro de téléphone"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:88
msgid "Please confirm your country code and enter your phone number."
msgstr ""
"Veuillez confirmer l'indicatif téléphonique et saisir votre numéro de "
"téléphone."

#: ../app/qml/pages/WaitRegistrationPage.qml:17
msgid "Enter your Name"
msgstr "Renseignez votre nom"

#: ../app/qml/pages/WaitRegistrationPage.qml:38
msgid "First Name"
msgstr "Prénom"

#: ../app/qml/pages/WaitRegistrationPage.qml:44
msgid "Last Name"
msgstr "Nom de famille"

#: ../app/qml/stores/AuthStateStore.qml:63
msgid "Invalid phone number!"
msgstr "Numéro de téléphone invalide !"

#: ../app/qml/stores/AuthStateStore.qml:70
msgid "Invalid code!"
msgstr "Code invalide !"

#: ../app/qml/stores/AuthStateStore.qml:77
msgid "Invalid password!"
msgstr "Mot de passe invalide !"

#: ../app/qml/stores/AuthStateStore.qml:97
msgid "Auth code not expected right now"
msgstr "Code d'authentification non prévu pour le moment"

#: ../app/qml/stores/AuthStateStore.qml:103
msgid "Oops! Internal error."
msgstr "Oups ! Une erreur interne s'est produite."

#: ../app/qml/stores/AuthStateStore.qml:122
msgid "Registration not expected right now"
msgstr "Inscription non prévue pour le moment"

#: ../app/qml/stores/ChatStateStore.qml:39
msgid "Error"
msgstr "Erreur"

#: ../app/qml/stores/ChatStateStore.qml:39
msgid "No valid location received after 180 seconds!"
msgstr "Aucune position valide reçue après 180 secondes !"

#: ../app/qml/stores/NotificationsStateStore.qml:12
msgid "Push Registration Failed"
msgstr "L'inscription aux notifications a échoué"

#: ../app/qml/stores/NotificationsStateStore.qml:23
msgid "No Ubuntu One"
msgstr "Aucune connexion à Ubuntu One"

#: ../app/qml/stores/NotificationsStateStore.qml:24
msgid "Please connect to Ubuntu One to receive push notifications."
msgstr "Veuillez vous connecter à Ubuntu One pour recevoir des notifications."

#: ../libs/qtdlib/chat/qtdchat.cpp:332
msgid "Draft:"
msgstr "Brouillon :"

#: ../libs/qtdlib/chat/qtdchat.cpp:613
msgid "is choosing contact..."
msgstr "choisit le contact..."

#: ../libs/qtdlib/chat/qtdchat.cpp:614
msgid "are choosing contact..."
msgstr "choisissent le contact..."

#: ../libs/qtdlib/chat/qtdchat.cpp:617
msgid "is choosing location..."
msgstr "choisit l'emplacement..."

#: ../libs/qtdlib/chat/qtdchat.cpp:618
msgid "are choosing location..."
msgstr "choisissent l'emplacement..."

#: ../libs/qtdlib/chat/qtdchat.cpp:623
msgid "is recording..."
msgstr "enregistre..."

#: ../libs/qtdlib/chat/qtdchat.cpp:624
msgid "are recording..."
msgstr "enregistrent..."

#: ../libs/qtdlib/chat/qtdchat.cpp:627
msgid "is typing..."
msgstr "est en train d'écrire..."

#: ../libs/qtdlib/chat/qtdchat.cpp:628
msgid "are typing..."
msgstr "sont en train d'écrire..."

#: ../libs/qtdlib/chat/qtdchat.cpp:631
msgid "is doing something..."
msgstr "fait quelque chose..."

#: ../libs/qtdlib/chat/qtdchat.cpp:632
msgid "are doing something..."
msgstr "font quelque chose..."

#: ../libs/qtdlib/messages/content/qtdmessageanimation.cpp:25
msgid "GIF"
msgstr "GIF"

#: ../libs/qtdlib/messages/content/qtdmessageanimation.cpp:25
msgid "GIF,"
msgstr "GIF,"

#: ../libs/qtdlib/messages/content/qtdmessagebasicgroupchatcreate.cpp:25
#: ../libs/qtdlib/messages/content/qtdmessagesupergroupchatcreate.cpp:23
msgid "created this group"
msgstr "a créé ce groupe"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:68
msgid "Call Declined"
msgstr "Appel refusé"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:71
msgid "Call Disconnected"
msgstr "Appel déconnecté"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:74
msgid "Call Ended"
msgstr "Appel terminé"

#. TRANSLATORS: This is a duration in hours:minutes:seconds format - only arrange the order, do not translate!
#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:78
msgid "%1:%2:%3"
msgstr "%1:%2:%3"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:79
msgid "Outgoing Call (%1)"
msgstr "Appel sortant (%1)"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:80
msgid "Incoming Call (%1)"
msgstr "Appel entrant (%1)"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:83
msgid "Cancelled Call"
msgstr "Appel annulé"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:83
msgid "Missed Call"
msgstr "Appel manqué"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:86
msgid "Call"
msgstr "Appel"

#: ../libs/qtdlib/messages/content/qtdmessagechataddmembers.cpp:34
msgid "added one or more members"
msgstr "un ou plusieurs membres ajoutés"

#: ../libs/qtdlib/messages/content/qtdmessagechataddmembers.cpp:34
msgid "joined the group"
msgstr "a rejoint le groupe"

#: ../libs/qtdlib/messages/content/qtdmessagechatchangephoto.cpp:20
msgid "changed the chat photo"
msgstr "a changé la photo du groupe"

#: ../libs/qtdlib/messages/content/qtdmessagechatchangetitle.cpp:19
msgid "changed the chat title"
msgstr "a changé le titre du groupe"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletemember.cpp:32
msgid "left the group"
msgstr "a quitté le groupe"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletemember.cpp:32
msgid "removed a member"
msgstr "a enlevé un membre"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletephoto.cpp:8
msgid "deleted the chat photo"
msgstr "a supprimé la photo du groupe"

#: ../libs/qtdlib/messages/content/qtdmessagechatjoinbylink.cpp:8
msgid "joined the group via the public link"
msgstr "a rejoint le groupe via le lien public"

#: ../libs/qtdlib/messages/content/qtdmessagechatsetttl.cpp:19
msgid "message TTL has been changed"
msgstr "la date d'expiration du message a été modifiée"

#: ../libs/qtdlib/messages/content/qtdmessagechatupgradefrom.cpp:29
#: ../libs/qtdlib/messages/content/qtdmessagechatupgradeto.cpp:29
msgid "upgraded to supergroup"
msgstr "transformé en supergroupe"

#: ../libs/qtdlib/messages/content/qtdmessagecontact.cpp:18
#, fuzzy
#| msgid "Contacts"
msgid "Contact"
msgstr "Contacts"

#: ../libs/qtdlib/messages/content/qtdmessagecontactregistered.cpp:9
msgid "has joined Telegram!"
msgstr "a rejoint Telegram !"

#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:19
msgid "Today"
msgstr "Aujourd'hui"

#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:21
msgid "Yesterday"
msgstr "Hier"

#. TRANSLATORS: String in date separator label. For messages within a week: full weekday name
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:24
msgid "dddd"
msgstr "dddd"

#. TRANSLATORS: String in date separator label. For messages of pas years: date number, month name and year
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:27
msgid "dd MMMM yyyy"
msgstr "dd MMMM yyyy"

#. TRANSLATORS: String in date separator label. For messages older that a week but within the current year: date number and month name
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:30
msgid "dd MMMM"
msgstr "dd MMMM"

#: ../libs/qtdlib/messages/content/qtdmessagelocation.cpp:19
msgid "Location"
msgstr "Position"

#: ../libs/qtdlib/messages/content/qtdmessagephoto.cpp:26
msgid "Photo"
msgstr "Photo"

#: ../libs/qtdlib/messages/content/qtdmessagephoto.cpp:26
msgid "Photo,"
msgstr "Photo,"

#: ../libs/qtdlib/messages/content/qtdmessagesticker.cpp:20
msgid "Sticker"
msgstr "Autocollant"

#: ../libs/qtdlib/messages/content/qtdmessagevideo.cpp:25
msgid "Video"
msgstr "Vidéo"

#: ../libs/qtdlib/messages/content/qtdmessagevideo.cpp:25
msgid "Video,"
msgstr "Vidéo,"

#: ../libs/qtdlib/messages/content/qtdmessagevideonote.cpp:30
msgid "Video message"
msgstr "Message vidéo"

#: ../libs/qtdlib/messages/content/qtdmessagevoicenote.cpp:31
msgid "Voice message"
msgstr "Message vocal"

#: ../libs/qtdlib/messages/content/qtdmessagevoicenote.cpp:31
msgid "Voice message,"
msgstr "Message vocal,"

#: ../libs/qtdlib/messages/qtdmessage.cpp:84
msgid "Me"
msgstr "Moi"

#: ../libs/qtdlib/messages/qtdmessagecontentfactory.cpp:96
msgid "Unimplemented:"
msgstr "Non implémenté :"

#: ../libs/qtdlib/messages/qtdmessagelistmodel.cpp:228
msgid "Unread Messages"
msgstr "Messages non lus"

#: ../libs/qtdlib/user/qtduserstatus.cpp:28
msgid "Last seen one month ago"
msgstr "Vu(e) pour la dernière fois il y a un mois"

#: ../libs/qtdlib/user/qtduserstatus.cpp:39
msgid "Last seen one week ago"
msgstr "Vu(e) pour la dernière fois il y a une semaine"

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "Last seen "
msgstr "Vu(e) pour la dernière fois le "

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "dd.MM.yy hh:mm"
msgstr "dd.MM.yy hh:mm"

#: ../libs/qtdlib/user/qtduserstatus.cpp:96
msgid "Seen recently"
msgstr "Vu(e) récemment"

#: ../push/pushhelper.cpp:124
msgid "sent you a message"
msgstr "vous a envoyé un message"

#: ../push/pushhelper.cpp:128
msgid "sent you a photo"
msgstr "vous a envoyé une photo"

#: ../push/pushhelper.cpp:132
msgid "sent you a sticker"
msgstr "vous a envoyé un autocollant"

#: ../push/pushhelper.cpp:136
msgid "sent you a video"
msgstr "vous a envoyé une vidéo"

#: ../push/pushhelper.cpp:140
msgid "sent you a document"
msgstr "vous a envoyé un document"

#: ../push/pushhelper.cpp:144
msgid "sent you an audio message"
msgstr "vous a envoyé un message audio"

#: ../push/pushhelper.cpp:148
msgid "sent you a voice message"
msgstr "vous a envoyé un message vocal"

#: ../push/pushhelper.cpp:152
msgid "shared a contact with you"
msgstr "a partagé un contact avec vous"

#: ../push/pushhelper.cpp:156
msgid "sent you a map"
msgstr "vous a envoyé une carte"

#: ../push/pushhelper.cpp:161
msgid "%1: %2"
msgstr "%1: %2"

#: ../push/pushhelper.cpp:166
msgid "%1 sent a message to the group"
msgstr "%1 a envoyé un message au groupe"

#: ../push/pushhelper.cpp:171
msgid "%1 sent a photo to the group"
msgstr "%1 a envoyé une photo au groupe"

#: ../push/pushhelper.cpp:176
msgid "%1 sent a sticker to the group"
msgstr "%1 a envoyé un autocollant au groupe"

#: ../push/pushhelper.cpp:181
msgid "%1 sent a video to the group"
msgstr "%1 a envoyé une vidéo au groupe"

#: ../push/pushhelper.cpp:186
msgid "%1 sent a document to the group"
msgstr "%1 a envoyé un document au groupe"

#: ../push/pushhelper.cpp:191
msgid "%1 sent a voice message to the group"
msgstr "%1 a envoyé un message vocal au groupe"

#: ../push/pushhelper.cpp:196
msgid "%1 sent a GIF to the group"
msgstr "%1 a envoyé un GIF au groupe"

#: ../push/pushhelper.cpp:201
msgid "%1 sent a contact to the group"
msgstr "%1 a envoyé un contact au groupe"

#: ../push/pushhelper.cpp:206
msgid "%1 sent a map to the group"
msgstr "%1 a envoyé une carte au groupe"

#: ../push/pushhelper.cpp:211 ../push/pushhelper.cpp:232
msgid "%1 invited you to the group"
msgstr "%1 vous a invité à rejoindre le groupe"

#: ../push/pushhelper.cpp:216
msgid "%1 changed group name"
msgstr "%1 a modifié le nom du groupe"

#: ../push/pushhelper.cpp:221
msgid "%1 changed group photo"
msgstr "%1 a changé la photo de groupe"

#. TRANSLATORS: Notification message saying: person A invited person B (to a group)
#: ../push/pushhelper.cpp:227
msgid "%1 invited %2"
msgstr "%1 a invité %2"

#: ../push/pushhelper.cpp:243
msgid "%1 removed you from the group"
msgstr "%1 vous a supprimé du groupe"

#: ../push/pushhelper.cpp:248
msgid "%1 has left the group"
msgstr "%1 a quitté le groupe"

#: ../push/pushhelper.cpp:253
msgid "%1 has returned to the group"
msgstr "%1 est revenu dans le groupe"

#. TRANSLATORS: This format string tells location, like: @ McDonals, New York
#: ../push/pushhelper.cpp:258
msgid "@ %1"
msgstr "@ %1"

#. TRANSLATORS: This format string tells who has checked in (in a geographical location).
#: ../push/pushhelper.cpp:260
msgid "%1 has checked-in"
msgstr "%1 est arrivé(e)"

#. TRANSLATORS: This format string tells who has just joined Telegram.
#: ../push/pushhelper.cpp:266
msgid "%1 joined Telegram!"
msgstr "%1 a rejoint Telegram !"

#: ../push/pushhelper.cpp:271 ../push/pushhelper.cpp:277
msgid "New login from unrecognized device"
msgstr "Nouvelle connexion à partir d'un appareil non connu"

#. TRANSLATORS: This format string indicates new login of: (device name) at (location).
#: ../push/pushhelper.cpp:276
msgid "%1 @ %2"
msgstr "%1 @ %2"

#: ../push/pushhelper.cpp:281
msgid "updated profile photo"
msgstr "photo de profil mise à jour"

#: ../push/pushhelper.cpp:286 ../push/pushhelper.cpp:291
#: ../push/pushhelper.cpp:296
msgid "You have a new message"
msgstr "Vous avez un nouveau message"

#: ../push/pushhelper.cpp:301
msgid "%1 pinned a message"
msgstr "%1 a épinglé un message"

#~ msgid "not available"
#~ msgstr "indisponible"

#~ msgid "Archived chats"
#~ msgstr "Discussions archivées"

#~ msgid "MMMM dd, yyyy"
#~ msgstr "dd MMMM yyyy"

#~ msgid "MMMM dd"
#~ msgstr "dd MMMM"

#~ msgid "Incorrect auth code length."
#~ msgstr "Longueur du code d'authentification incorrecte."

#~ msgid "Phone call"
#~ msgstr "Appel téléphonique"

#~ msgid "sent an audio message"
#~ msgstr "a envoyé un message audio"

#~ msgid "sent a photo"
#~ msgstr "a envoyé une photo"

#~ msgid "sent a video"
#~ msgstr "a envoyé une vidéo"

#~ msgid "sent a video note"
#~ msgstr "a envoyé une note vidéo"

#~ msgid "sent a voice note"
#~ msgstr "a envoyé une note vocale"

#~ msgid "joined by invite link"
#~ msgstr "vous a rejoint via un lien d'invitation"

#~ msgid "Image"
#~ msgstr "Image"

#~ msgid "File"
#~ msgstr "Fichier"

#~ msgid "Audio"
#~ msgstr "Audio"

#~ msgid "sent an unknown message: %1"
#~ msgstr "a envoyé un message inconnu : %1"

#~ msgid "Status:"
#~ msgstr "Statut : "

#, fuzzy
#~| msgid "dd MMMM"
#~ msgid "dd MM"
#~ msgstr "dd MMMM"
